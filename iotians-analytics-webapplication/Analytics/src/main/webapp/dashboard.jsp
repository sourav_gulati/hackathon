<%-- 
    Document   : dashboard
    Created on : 19 Feb, 2016, 7:40:09 PM
    Author     : kislay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html><head>
        <meta charset="utf-8">
        <title>IOTTian's Analytics DashBoard </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Carlos Alvarez - Alvarez.is">

        <!-- Le styles -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/main.css" rel="stylesheet">
        <link href="assets/css/font-style.css" rel="stylesheet">
        <link href="assets/css/flexslider.css" rel="stylesheet">

        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

        <style type="text/css">
            body {
                padding-top: 60px;
            }
        </style>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

        <!-- Google Fonts call. Font Used Open Sans & Raleway -->
        <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

        <script type="text/javascript">
          
            $(document).ready(function () {

                $("#btn-blog-next").click(function () {
                    $('#blogCarousel').carousel('next')
                });
                $("#btn-blog-prev").click(function () {
                    $('#blogCarousel').carousel('prev')
                });

                $("#btn-client-next").click(function () {
                    $('#clientCarousel').carousel('next')
                });
                $("#btn-client-prev").click(function () {
                    $('#clientCarousel').carousel('prev')
                });

            });

            $(window).load(function () {
                $.get("http://techiebit.com:8080/statistics", function (data) {
                    $(".result").html(data);
                    var innerData1 = data.STATISTICAL_DATA;

                    for (var i = 0; i < innerData1.length; i++) {
                        var avegargehumid = innerData1[i].avgHumidity;
                        $('#averagehumidity').html(avegargehumid);
                        $('#minhumidity').html(innerData1[i].minHumidity);
                        $('#maxhumidity').html(innerData1[i].maxHumidity);
                        $('#averagetemperature').html(innerData1[i].avgTemperature);
                        $('#mintemperature').html(innerData1[i].minTemperature);
                        $('#maxtemperature').html(innerData1[i].maxTemperature);


                        //chart.series[1].addPoint(innerData1[i].humidity, true, true);
                        break;
                    }
                });
                
                $("#checkonoff").click(function() {
                   alert('hey'); 
                });
            });


            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    slideshow: true,
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });
            var statebulb = off;
            function toggleCall() {
                alert('test0');
            }

            $("#checkonoff").click(function () {
                //$("p").toggle();
                alert('test0');
            });



        </script>



    </head>
    <body>

        <!-- NAVIGATION MENU -->

        <div class="navbar-nav navbar-inverse navbar-fixed-top" >
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="assets/img/logoiotians.png" width="75px;" alt=""> Analytics Dashboard</a>
                </div> 
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">



                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container" style="margin-top: 50px;">



            <!-- SECOND ROW OF BLOCKS -->     
            <div class="row">

                <!-- GRAPH CHART - lineandbars.js file -->   
              
                <div class="col-sm-10">
                       <dtitle style="">Real Time Humidity</dtitle>
                    <div class="dash-unit">
                         <dtitle style="">Real Time Humidity</dtitle>
                        <hr>
                        <div class="section-graph"><div id="humidityValue">Current Value</div>
                            <div id="importantchart"></div>
                            <br>
                            <div class="graph-info">
                                <i class="graph-arrow"></i>
                                <span class="graph-info-big">Humidity</span>
                                <span class="graph-info-small"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="dash-unit">
                        <dtitle>Stats</dtitle>
                        <div class="section-graph">
                            <div class="graph-info">
                                <span class="graph-info-small"> Min Humidity <div id="minhumidity"></div>  </span><br>
                                <span class="graph-info-small" >Max Humidity: <div id="maxhumidity"></div>  </span><br>
                                <span class="graph-info-small"> Average Humidity: <div id="averagehumidity"></div>  </span>

                                <span class="graph-info-small"> Min Temperature: <div id="mintemperature"></div>  </span><br>
                                <span class="graph-info-small" >Max Temperature: <div id="maxtemperature"></div>  </span><br>
                                <span class="graph-info-small"> Average Temperature: <div id="averagetemperature"></div>  </span>

                            </div>
                        </div>
                    </div>   


                </div>



                <!-- 30 DAYS STATS - CAROUSEL FLEXSLIDER -->     

            </div><!-- /row -->
            <div class='row'>
                 <div class="col-sm-10">
                       <dtitle style="">Real Time Temperature</dtitle>
                    <div class="dash-unit">
                         <dtitle style="">Real Time Temperature</dtitle>
                        <hr>
                        <div class="section-graph"><div id="temperatureValue">Current Value</div>
                            <div id="importanttempchart"></div>
                            <br>
                            <div class="graph-info">
                                <i class="graph-arrow"></i>
                                <span class="graph-info-big">Temperature</span>
                                <span class="graph-info-small"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

            <div class="row">
                <div class="col-sm-2">
                    <input type="button" id="checkonoff" value="Toggle">
                </div>
                <div class="col-sm-10"></div>
            </div>


        </div>


        <!-- THIRD ROW OF BLOCKS -->     
        <!-- /row -->

        <!--/row -->     

        <!--/row -->



    </div> <!-- /container -->
    <div id="footerwrap">
        <footer class="clearfix"></footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <p><img src="assets/img/logo.png" alt=""></p>
                    <p>Team IOTians - Crafted With Love..</p>
                </div>

            </div><!-- /row -->
        </div><!-- /container -->		
    </div><!-- /footerwrap -->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/lineandbars.js"></script>

    <!--<script type="text/javascript" src="assets/js/dash-charts.js"></script>
    <script type="text/javascript" src="assets/js/gauge.js"></script>
    -->
    <!-- NOTY JAVASCRIPT -->
    <!--	<script type="text/javascript" src="assets/js/noty/jquery.noty.js"></script>
            <script type="text/javascript" src="assets/js/noty/layouts/top.js"></script>
            <script type="text/javascript" src="assets/js/noty/layouts/topLeft.js"></script>
            <script type="text/javascript" src="assets/js/noty/layouts/topRight.js"></script>
            <script type="text/javascript" src="assets/js/noty/layouts/topCenter.js"></script>
    -->
    <script src="assets/js/jquery.flexslider.js" type="text/javascript"></script>
    <!-- You can add more layouts if you want -->
    <script type="text/javascript" src="assets/js/noty/themes/default.js"></script>
    //    <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
    <script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
    <script src="assets/js/jquery.flexslider.js" type="text/javascript"></script>
    <%
        //	<!-- You can add more layouts if you want -->
        //	<script type="text/javascript" src="assets/js/noty/themes/default.js"></script>
        //    <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
        //	<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
        //	<script src="assets/js/jquery.flexslider.js" type="text/javascript"></script>
    %>
    <script type="text/javascript" src="assets/js/admin.js"></script>

</body></html>