﻿	// LINE AND BARS CHARTS

$(function () {
  
  function generateNumber(min, max) {
    min = typeof min !== 'undefined' ? min : 1;
    max = typeof max !== 'undefined' ? max : 100;
    
    return Math.floor((Math.random() * max) + min);
    //return 100;
  }
  
  var chart,
      categories = ['Categorie 1', 'Categorie 2', 'Categorie 3', 'Categorie 4', 'Categorie 5','Categorie 6', 'Categorie 7', 'Categorie 8', 'Categorie 9', 'Categorie 10', 'Categorie 11', 'Categorie 12', 'Categorie 13', 'Categorie 14', 'Categorie 15', 'Categorie 16', 'Categorie 17', 'Categorie 18', 'Categorie 19','Categorie 20', 'Categorie 21','Categorie 22', 'Categorie 23', 'Categorie 24', 'Categorie 25', 'Categorie 26', 'Categorie 27', 'Categorie 28', 'Categorie 29', 'Categorie 30'],
      serie1 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      serie2 = [0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      $aapls;
      
      
var chartTemp,
      categories = ['Categorie 1', 'Categorie 2', 'Categorie 3', 'Categorie 4', 'Categorie 5','Categorie 6', 'Categorie 7', 'Categorie 8', 'Categorie 9', 'Categorie 10', 'Categorie 11', 'Categorie 12', 'Categorie 13', 'Categorie 14', 'Categorie 15', 'Categorie 16', 'Categorie 17', 'Categorie 18', 'Categorie 19','Categorie 20', 'Categorie 21','Categorie 22', 'Categorie 23', 'Categorie 24', 'Categorie 25', 'Categorie 26', 'Categorie 27', 'Categorie 28', 'Categorie 29', 'Categorie 30'],
      serie1 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
      serie2 = [0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      $aapls;      
  //importanttempchart
  
  
  $(document).ready(function() {
      
        chartTemp = new Highcharts.Chart({
      chart: {
        renderTo: 'importanttempchart',
        type: 'column',
        backgroundColor: 'transparent',
        height: 140,
        marginLeft: 3,
        marginRight: 3,
        marginBottom: 0,
        marginTop: 0,
        
      },
      title: {
        text: ''
      },
      xAxis: {
        lineWidth: 0,
        tickWidth: 0,
        labels: { 
          enabled: true 
        },
         title: {
          text: 'time',
        },
        categories: categories
      },
      yAxis: {
        labels: { 
          enabled: true 
        },
        gridLineWidth: 0,
        title: {
          text: 'Data',
        },
      },
      series: [{
        name: 'Temperature',
        data: serie1
      }, {
        name: 'Humidity',
        color: 'red',
        type: 'line',
        data: serie2
      }],
      credits: { 
        enabled: false 
      },
      legend: { 
        enabled: false 
      },
      plotOptions: {
        column: {
          borderWidth: 0,
          color: '#b2c831',
          shadow: false
        },
        line: {
          marker: { 
            enabled: false 
          },
          lineWidth: 3
        }
      },
      tooltip: { 
        enabled: false
      }
    });

    chart = new Highcharts.Chart({
      chart: {
        renderTo: 'importantchart',
        type: 'column',
        backgroundColor: 'transparent',
        height: 140,
        marginLeft: 3,
        marginRight: 3,
        marginBottom: 0,
        marginTop: 0
      },
      title: {
        text: ''
      },
      xAxis: {
        lineWidth: 0,
        tickWidth: 0,
        labels: { 
          enabled: true 
        },
         title: {
          text: 'time',
        },
        categories: categories
      },
      yAxis: {
        labels: { 
          enabled: true 
        },
        gridLineWidth: 0,
        title: {
          text: 'Data',
        },
      },
      series: [{
        name: 'Temperature',
        data: serie1
      }, {
        name: 'Humidity',
        color: 'yellow',
        type: 'line',
        data: serie2
      }],
      credits: { 
        enabled: false 
      },
      legend: { 
        enabled: false 
      },
      plotOptions: {
        column: {
          borderWidth: 0,
          color: '#b2c831',
          shadow: false
        },
        line: {
          marker: { 
            enabled: false 
          },
          lineWidth: 3
        }
      },
      tooltip: { 
        enabled: false
      }
    });
      
    setInterval(function() {
      $.get("http://techiebit.com:8080/realtime", function (data) {$(".result").html(data);
          var innerData = data.HISTORICAL_DATA;
         // alert(innerData);
          
          for(var i=0; i<innerData.length; i++){
              $('#humidityValue').html('Humidity: '+innerData[i].humidity);
              $('#temperatureValue').html('Temperature: '+innerData[i].temperature);
              chartTemp.series[1].addPoint(innerData[i].temperature, true, true);
              chartTemp.series[0].addPoint(0, true, true);
              chart.series[0].addPoint(0, true, true);
              chart.series[1].addPoint(innerData[i].humidity, true, true);
              
              break;
          }
      });
      
      
    }, 750);
    
    
  
    setInterval(function() {
      $('.info-aapl span').each(function(index, elem) {
        $(elem).animate({
          height: generateNumber(1, 40)
        });
      });

    }, 3000);
  });
  
});
