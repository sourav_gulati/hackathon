package com.iotians.rest.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.iotians.rest.constants.RestConstants;

@RestController
@ComponentScan("com.iotians.rest")
public class SensorController {

	private Cluster cluster;
	private Session session;
	private PreparedStatement psInsertSensor;
	private PreparedStatement psHistorySensor;
	private PreparedStatement psStatsSensor;
	private Properties properties;
	private InputStream inputStream;
	private Connection connection;
	private java.sql.Statement hiveStatement;
	private static String hiveInsertStatemt = "insert into iot_keyspace.sensor(device_id,event_time,humidity,tempertaure) VALUES ('inputDevID',inputEventTime,inuptHumid,inputTemp)";

	@PostConstruct
	private void init() {
		String cassandraHost = null;
		String hiveHost = null;

		String hivePort = null;

		try {

			properties = new Properties();
			inputStream = new FileInputStream("/root/config.properties");
			properties.load(inputStream);
			cassandraHost = properties.getProperty("cassandra.host");
			hiveHost = properties.getProperty("hive.host");
			hivePort = properties.getProperty("hive.port");

			System.out.println("Cassandra Host: " + cassandraHost);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		cluster = Cluster.builder().addContactPoint(cassandraHost).build();
		session = cluster.connect();

		psInsertSensor = session.prepare(
				"insert into iot_keyspace.sensor (device_id,event_time,humidity,tempertaure,moisture) VALUES (?,?,?,?,?)");

		psHistorySensor = session.prepare("select * from iot_keyspace.sensor where device_id='1010' limit 1");

		psStatsSensor = session.prepare("select * from iot_keyspace.sensor_stats where device_id='1010'");

		// initHive(hiveHost, hivePort);
	}

	// public void initHive(String host, String port) {
	//
	// try {
	// Class.forName("org.apache.hive.jdbc.HiveDriver");
	// connection = DriverManager.getConnection("jdbc:hive2://" + host
	// + ":" + port, "", "");
	//
	// hiveStatement = connection.createStatement();
	// } catch (ClassNotFoundException e) {
	// System.err.println("Hive Driver not found " + e);
	// } catch (SQLException e) {
	// System.err.println("Error while getting connection " + e);
	// }
	//
	// }

	@RequestMapping(value = "/device/data", method = RequestMethod.GET)
	public String writeSensorData(@RequestParam("deviceId") String deviceId,
			@RequestParam("temperature") Double temperature, @RequestParam("humidity") Double humidity,
			@RequestParam("moisture") Double moisture) {
		long eventTime = System.currentTimeMillis();
		System.out.println("EventTime: " + eventTime + ", Temperature: " + temperature + "& Humidity: " + humidity+ " & moisture "+moisture);
		session.execute(psInsertSensor.bind(deviceId, eventTime, humidity, temperature,moisture));
		System.out.println("Data Inserted Succesfully in Cassandra");
		// String hiveStatement2 = getHiveStatement(deviceId,
		// String.valueOf(eventTime), String.valueOf(temperature),
		// String.valueOf(humidity));
		// try {
		// hiveStatement.executeQuery(hiveStatement2);
		// } catch (SQLException e) {
		// System.err.println("Error while inserting to hive: " + e);
		// }

		return "Data Inserted Succesfully";
	}

	public String getHiveStatement(String deviceId, String eventTime, String temperature, String humidity) {

		return hiveInsertStatemt.replace("inputDevID", deviceId).replace("inputEventTime", eventTime)
				.replace("inputTemp", temperature).replace("inuptHumid", humidity);
	}

	@RequestMapping(value = "/realtime", method = RequestMethod.GET)
	public JSONObject getRealtimeData() {
		BoundStatement historyData = psHistorySensor.bind();

		JSONObject jsonObject = new JSONObject();
		jsonObject.put(RestConstants.HISTORICAL_DATA, getRealtime(historyData));
		return jsonObject;
	}

	public JSONArray getRealtime(Statement statement) {

		ResultSet result = session.execute(statement);
		JSONArray jsonArray = new JSONArray();
		for (Row row : result) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("deviceId", row.getString(RestConstants.COL_NAME_DEVICE_ID));
			jsonObject.put("eventTime", row.getLong(RestConstants.COL_NAME_EVENT_TIME));
			jsonObject.put("humidity", row.getDouble(RestConstants.COL_NAME_HUMIDITY));
			jsonObject.put("moisture", row.getDouble(RestConstants.COL_NAME_MOISTURE));
			jsonObject.put("temperature", row.getDouble(RestConstants.COL_NAME_TEMPERTAURE));
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}

	// hp_probook_440_g2_notebook_pc
	// dell_inspiron_11_2-in-1_3148_touchscreen_laptop
	// amul_butter
	// maggi_veg_atta_noodles
	// iphone_6s_gold
	// samsung_galaxy_s6_edge
	// nature_fresh_sampoorna_chakki_atta
	// fortune_refined_soyabean_oil
	// samsung_galaxy_note_5
	// moto_g_3rd_generation
	// apple_ipod_touch
	// sony_bravia_40_inches_led_tv
	// lg_32_litres_mc3283pmpg_microwave_oven_convection
	// videocon_quanta_plus_semi_automatic_washing_machine

	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public JSONObject getStatisticalData() {

		BoundStatement statisticalData = psStatsSensor.bind();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(RestConstants.STATISTICAL_DATA, getStatistical(statisticalData));
		return jsonObject;
	}

	public JSONArray getStatistical(Statement statement) {
		ResultSet result = session.execute(statement);
		JSONArray jsonArray = new JSONArray();
		for (Row row : result) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("deviceId", row.getString("device_id"));
			jsonObject.put("maxHumidity", row.getDouble("max_humidity"));
			jsonObject.put("minHumidity", row.getDouble("min_humidity"));
			jsonObject.put("avgHumidity", row.getDouble("avg_humidity"));
			jsonObject.put("maxTemperature", row.getDouble("max_tempertaure"));
			jsonObject.put("minTemperature", row.getDouble("min_tempertaure"));
			jsonObject.put("avgTemperature", row.getDouble("avg_tempertaure"));
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}
}
