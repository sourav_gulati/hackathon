package com.iotians.rest.constants;

public class RestConstants {

	public static final String RECOMMENDED_OFFERS = "RECOMMENDED_OFFERS";
	public static final String GENERAL_OFFERS = "GENERAL_OFFERS";
	public static final String COL_NAME_OFFERS = "offers";

	public static final String COL_NAME_CURRENT_STATUS = "current_status";

	public static final String HISTORICAL_DATA = "HISTORICAL_DATA";
	public static final String STATISTICAL_DATA = "STATISTICAL_DATA";
	public static final String COL_NAME_DEVICE_ID = "device_id";
	public static final String COL_NAME_EVENT_TIME = "event_time";
	public static final String COL_NAME_TEMPERTAURE = "tempertaure";
	public static final String COL_NAME_HUMIDITY = "humidity";
	public static final String COL_NAME_MOISTURE = "moisture";

}
