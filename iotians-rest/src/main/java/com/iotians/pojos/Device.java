package com.iotians.pojos;

public class Device {

	private String userUniqueId;
	private String uuid;
	private String uuidMajor;
	private String uuidMinor;

	public String getUserUniqueId() {
		return userUniqueId;
	}

	public void setUserUniqueId(String userUniqueId) {
		this.userUniqueId = userUniqueId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUuidMajor() {
		return uuidMajor;
	}

	public void setUuidMajor(String uuidMajor) {
		this.uuidMajor = uuidMajor;
	}

	public String getUuidMinor() {
		return uuidMinor;
	}

	public void setUuidMinor(String uuidMinor) {
		this.uuidMinor = uuidMinor;
	}

	@Override
	public String toString() {
		return "Device [userUniqueId=" + userUniqueId + ", uuid=" + uuid
				+ ", uuidMajor=" + uuidMajor + ", uuidMinor=" + uuidMinor + "]";
	}

}
