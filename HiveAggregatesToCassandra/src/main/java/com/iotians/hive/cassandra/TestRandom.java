package com.iotians.hive.cassandra;

import java.util.Random;

public class TestRandom {
	public static void main(String[] args) {
		Random rand =new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	   while(true)
	   {
		int randomNum = rand.nextInt((26 - 24) + 1) + 23;

	    System.out.println(randomNum);
	   }
	}
}
