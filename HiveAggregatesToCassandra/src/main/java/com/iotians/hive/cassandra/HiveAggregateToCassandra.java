package com.iotians.hive.cassandra;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;

public class HiveAggregateToCassandra {

	private static String hiveSelectQuery = "select device_id,min(tempertaure) as min_temp ,max(tempertaure) as max_temp ,avg(tempertaure) as avg_temp ,min(humidity) as min_humidity,max(humidity) as max_humidity ,avg(humidity) as avg_humidity from iot_keyspace.sensor group by device_id";
	private static String cassInsertQuery = "insert into iot_keyspace.sensor_stats(device_id,avg_humidity,avg_tempertaure,max_humidity,max_tempertaure,min_humidity,min_tempertaure) VALUES(?,?,?,?,?,?,?)";

	private Cluster cluster;
	private Session session;
	private Connection connection;
	private java.sql.Statement hiveStatement;
	private PreparedStatement cassInsertStatement;
	private Properties properties;

	private void initializeProps() {
		properties = new Properties();
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(
					"/home/ubuntu/config/config.properties");

			properties.load(inputStream);

		} catch (FileNotFoundException e) {
			System.err.println("Property file not found" + e);
			throw new RuntimeException("Property file not found");
		} catch (IOException e) {
			System.err.println("Error while loading property file" + e);
			throw new RuntimeException("Error while loading property file");
		}
	}

	private void initializeCass() {
		cluster = Cluster.builder()
				.addContactPoint(properties.getProperty("cassandra.host"))
				.build();
		session = cluster.connect();
		cassInsertStatement = session.prepare(cassInsertQuery);
	}

	private void initializeHive() {
		try {

			Class.forName("org.apache.hive.jdbc.HiveDriver");

			connection = DriverManager.getConnection(
					"jdbc:hive2://" + properties.getProperty("hive.host") + ":"
							+ properties.getProperty("hive.port"), "", "");
			hiveStatement = connection.createStatement();
		} catch (SQLException e) {
			System.err.println("Error while creating prepared statement" + e);
			throw new RuntimeException("Error while creating prepared statement");
		} catch (ClassNotFoundException e) {
			System.err.println("Hive Driver Not Found" + e);
			throw new RuntimeException("Hive Driver Not Found");
		}

	}
	
	
	private void publishCassFromHive()
	{
		ResultSet hiveRs=null;
		try {
			System.out.println(hiveSelectQuery);
			hiveRs = hiveStatement.executeQuery(hiveSelectQuery);
			
		} catch (SQLException e) {
			System.err.println("Error while executing hive query"+e);
		}
		
		try {
		if(hiveRs.next())
		{
		Object [] objectArray =new Object[7];
		
		
			
			objectArray[0]=hiveRs.getString("device_id");
			objectArray[1]=new Double(hiveRs.getDouble("avg_humidity"));
			objectArray[2]=new Double(hiveRs.getDouble("avg_temp"));
			objectArray[3]=new Double(hiveRs.getDouble("max_humidity"));
			objectArray[4]=new Double(hiveRs.getDouble("max_temp"));
			objectArray[5]=new Double(hiveRs.getDouble("min_humidity"));
			objectArray[6]=new Double(hiveRs.getDouble("min_temp"));
			
			
		
		
		
		
		
		session.execute(cassInsertStatement.bind(objectArray));
		System.out.println("Inserted data to cassandra");
		}
		} catch (SQLException e) {
			System.err.println("Error while retrieving hive objects"+e);
		}
		finally{
			session.close();
			cluster.close();
		}
	}

	public static void main(String[] args) {
		
		HiveAggregateToCassandra hiveAggregateToCassandra = new HiveAggregateToCassandra();
		hiveAggregateToCassandra.initializeProps();
		hiveAggregateToCassandra.initializeHive();
		hiveAggregateToCassandra.initializeCass();
		hiveAggregateToCassandra.publishCassFromHive();
		
		
	}

}
