myApp.controller("PantryController", function($scope, json, AdobeOffice){
    var pantryMenu=null;
    var mealData = json.data;

    for(var item in mealData){
        if(mealData[item].location == AdobeOffice.NOIDA125){
            pantryMenu=mealData[item].menu;
            break;
        }
    }

    if(pantryMenu==null){
        pantryMenu=mealData[0].menu;
    }

    $scope.pantryMenu = pantryMenu;

    TTS.speak({
           text: "Welcome to food section.",
           locale: 'en-GB',
           rate: 1.5
       }, function () {
           // Do Something after success
       }, function (reason) {
           // Handle the error case
       });
});
