myApp.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

            .state('lunch', {
                url: '/lunch',
                cache: 'false',
                templateUrl: 'modules/menu/lunch/lunch.html',
                controller: "LunchController",
                resolve: {
                    json: function (RetrieveFileService) {
                        //return RetrieveFileService.retrieveJSONFile('menu.json');
                        return RetrieveFileService.retrieveJSONFile("http://52.77.33.142:8080/items/get?json={%22userUniqueId%22:%20%229654867097%22,%20%22uuid%22:%20%22f7826da6-4fa2-4e98-8024-bc5b71e0893e%22,%20%22uuidMajor%22:%20%2249769%22,%20%22uuidMinor%22:%20%2221644%22}");
                    }
                }
            })

            .state('pantry', {
                url: '/pantry',
                cache: 'false',
                templateUrl: 'modules/menu/pantry/pantry.html',
                controller: "PantryController",
                resolve: {
                    json: function (RetrieveFileService) {
                        return RetrieveFileService.retrieveJSONFile('pantry_options.json');
                    }
                }
            })

            .state('electronic', {
                url: '/electronics',
                cache: 'false',
                templateUrl: 'modules/menu/electronics/electronics.html',
                controller: "ElectronicsController",
                resolve: {
                    json: function (RetrieveFileService) {
                       // return RetrieveFileService.retrieveJSONFile('electronics.json');
                        return RetrieveFileService.retrieveJSONFile("http://52.77.33.142:8080/items/get?json={%22userUniqueId%22:%20%229654867097%22,%20%22uuid%22:%20%22f7826da6-4fa2-4e98-8024-bc5b71e0893e%22,%20%22uuidMajor%22:%20%2223330%22,%20%22uuidMinor%22:%20%2238648%22}");
                        //"http://52.77.33.142:8080/items/get?json={%22userUniqueId%22:%20%229654867097%22,%20%22uuid%22:%20%22f7826da6-4fa2-4e98-8024-bc5b71e0893e%22,%20%22uuidMajor%22:%20%2223330%22,%20%22uuidMinor%22:%20%2238648%22}"
                    }
                }
            })

            .state('fooditems', {
                url: '/pantry',
                cache: 'false',
                templateUrl: 'modules/menu/pantry/pantry.html',
                controller: "PantryController",
                resolve: {
                    json: function (RetrieveFileService) {
                        return RetrieveFileService.retrieveJSONFile('pantry_options.json');
                    }
                }
            })

            .state('welcome', {
                url: "/welcome",
                templateUrl: 'modules/welcome/welcome.html'
            });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise(function () {
        return '/welcome';
    });
});
